__TS__FunctionCall = function(fn, thisArg, ...)
	local args = ({ ... });
	return fn(thisArg, (unpack or table.unpack)(args));
end;