const concat = require('concat');
let appRoot = require("app-root-path");
const Args = process.argv;
const fs = require("fs");
appRoot = appRoot + "\\";
const TocPath = "Source\\Source.toc";
class NiceDate extends Date {
    constructor() {
        super();
    }
    GetDay() {
        const Month = this.getMonth() + 1; // getMonth() is zero-based
        const Day = this.getDate();
        return [
            (Day > 9 ? "" : "0") + Day,
            " / ",
            (Month > 9 ? "" : "0") + Month,
            " / ",
            this.getFullYear()
        ].join("");
    }
    GetTime() {
        const Hour = this.getHours();
        const Minute = this.getMinutes();
        const Seconds = this.getSeconds();
        const MilliSeconds = this.getMilliseconds();
        return "<" + [
            (Hour > 9 ? "" : "0") + Hour,
            ":",
            (Minute > 9 ? "" : "0") + Minute,
            ":",
            (Seconds > 9 ? "" : "0") + Seconds,
            ":",
            (MilliSeconds > 99 ? "" : MilliSeconds > 9 ? "0" : "00") + MilliSeconds,
        ].join("") + ">";
    }
}
function GetNiceTime() {
    return new NiceDate().GetTime();
}
var Core;
(function (Core) {
    let Jobs = [];
    async function GetJobs() {
        if (Jobs.length == 0) {
            const TocFilePromise = ReadFile("ReadToc", TocPath, "UTF-8");
            await TocFilePromise.then((value) => {
                // Here we got the content of the toc, we will add every lines by splitting
                const FileContent = value[1];
                const FileLines = FileContent.split(RegExp("[\r\n]"));
                for (let i = 0; i < FileLines.length; i++) {
                    const ThisLine = FileLines[i];
                    const jobRegex = ThisLine.match(RegExp(/(Qt[\w]*).ts/));
                    if (jobRegex) {
                        Jobs.push(jobRegex[1]);
                    }
                }
            });
        }
        return Jobs;
    }
    Core.BaseDirectory = Args[1];
    Core.LuaFilesPaths = [
        "Header"
    ];
    //#region Files Management
    async function AppendFile(filePath, fileContent) {
        return await fs.appendFile(filePath, fileContent, (err) => {
            if (err) {
                console.log(GetNiceTime(), "AppendFile failed for " + filePath + " Error: " + err);
            }
        });
    }
    async function DeleteFile(filePath) {
        return await fs.unlink(filePath, (err) => {
            if (err) {
                console.log(GetNiceTime(), "DeleteFile failed for " + filePath + " Error: " + err);
            }
        });
    }
    async function ReadFile(identifier, filePath, enc = "UTF-8") {
        // make Promise version of fs.readFile()
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, enc, (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve([identifier, data]);
                }
            });
        });
    }
    async function WriteFile(filePath, fileContent) {
        await fs.writeFile(filePath, fileContent, (err) => {
            if (err) {
                console.log(GetNiceTime(), "WriteFile failed for" + filePath + " Error: " + err);
            }
        });
    }
    async function GetFiles(identifier, filePath, enc = "UTF-8") {
        // make Promise version of fs.readFile()
        return new Promise((resolve, reject) => {
            fs.readdir(filePath, enc, (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve([identifier, data]);
                }
            });
        });
    }
    //#endregion
    /** Call it after tstl transpiled */
    async function LuaWrapper() {
        console.log(GetNiceTime(), "Typescript-To-Lua - Transpilation finished");
        console.log(GetNiceTime(), "Lua Wrapper - Begins");
        const AddonFolder = appRoot + "AddOn\\";
        // Empty addons directory        
        await GetFiles("AddonFolderFiles", AddonFolder).then(async ([Identifier, TypeScriptFilesPaths]) => {
            console.log(GetNiceTime(), "TypeScript Wrapper - Finished reading Toc", TypeScriptFilesPaths.length, "files were found.");
            // For each files, remove the top of the file and add them to a single file
        });
        await GetJobs();
        // const TocFile = AddonFolder + SavedAddonName + ".toc";
        try {
            const FilesToMerge = [
                "Other/Outputs/Lua/Header.lua",
                "Other/Outputs/Lua/Source.lua",
                "Other/Outputs/Lua/Footer.lua"
            ];
            let Content = '';
            let ClassIndexes = {};
            for (let i = 0; i < FilesToMerge.length; i++) {
                const ReadFileResult = await ReadFile(FilesToMerge[i], FilesToMerge[i]);
                Content += "\n\n-- " + ReadFileResult[0] + " --\n";
                Content += ReadFileResult[1];
            }
            Content = Content.replace('return ____exports', '');
            // Copy Header and Footer to every file
            // Use specific LUA syntax to determine where to start and finish 
            // From job to job.
            // const ContentSplit = Content.split(RegExp(/\r?\n/));
            // Check all matches, put names and index into dict
            const JobIndexes = [];
            for (let j = 0; j < Jobs.length; j++) {
                const thisJob = Jobs[j];
                const jobMatch = Content.match(RegExp(`local (${thisJob}) = {}`));
                if (jobMatch) {
                    console.log(jobMatch[1], jobMatch.index);
                    JobIndexes.push({ name: jobMatch[1], index: jobMatch.index });
                }
            }
            // Substring the content out using index and index + 1
            let jobContent = [];
            for (let i = 0; i < JobIndexes.length; i++) {
                const thisJobIndex = JobIndexes[i];
                const nextJob = i < JobIndexes.length ? JobIndexes[i + 1] : undefined;
                let jobSubstring = Content.substring(thisJobIndex.index, nextJob ? nextJob.index : undefined);
                jobSubstring += `\nreturn ${thisJobIndex.name}`;
                jobContent.push({ name: thisJobIndex.name, content: jobSubstring });
            }
            console.log(GetNiceTime(), "Lua Addon Wrapper - Write");
            if (jobContent.length > 0) {
                // Put header into all files!
                const header = Content.substring(0, JobIndexes[0].index);
                for (let i = 0; i < jobContent.length; i++) {
                    const luaContent = `${header}\n${jobContent[i].content}`;
                    const WrappedLuaFile = `AddOn\\${jobContent[i].name}.lua`;
                    // Lua File
                    await WriteFile(WrappedLuaFile, luaContent)
                        .then(async () => {
                        console.log(`${GetNiceTime()}, ${WrappedLuaFile} Write Finished!`);
                    });
                }
            }
            console.log(GetNiceTime(), "Lua Addon Wrapper - Finished", Date());
        }
        catch (error) {
            console.log(GetNiceTime(), "Error", error);
        }
    }
    Core.LuaWrapper = LuaWrapper;
    async function GetTypeScriptFilesPaths() {
        const PathsArray = [];
        const TocFilePromise = ReadFile("ReadToc", TocPath, "UTF-8");
        await TocFilePromise.then((value) => {
            // Here we got the content of the toc, we will add every lines by splitting
            const FileContent = value[1];
            const FileLines = FileContent.split(RegExp("[\r\n]"));
            for (let i = 0; i < FileLines.length; i++) {
                const ThisLine = FileLines[i];
                if (ThisLine != "" && ThisLine.indexOf("##") == -1 && ThisLine.indexOf("AddonName=") == -1) {
                    PathsArray.push("Source/" + ThisLine);
                }
            }
        });
        return PathsArray;
    }
    async function TypeScriptWrapper() {
        await GetJobs();
        console.log(GetNiceTime(), "TypeScript Wrapper - Started");
        // Gather the files paths array from the toc file
        const TypeScriptFilesPaths = await GetTypeScriptFilesPaths();
        console.log(TypeScriptFilesPaths);
        let LuaContent = "";
        console.log(GetNiceTime(), "TypeScript Wrapper - Finished reading Toc", TypeScriptFilesPaths.length, "files were found.");
        // For each files, remove the top of the file and add them to a single file
        for (let i = 0; i < TypeScriptFilesPaths.length; i++) {
            const ThisFilePath = TypeScriptFilesPaths[i];
            await ReadFile(ThisFilePath, ThisFilePath).then((value) => {
                const ContentBegins = value[1].indexOf("// Content //");
                let Sanitized = ContentBegins != -1 ? value[1].substring(ContentBegins + 13) : value[1];
                LuaContent += "\n//" + ThisFilePath + "//\n\r" + Sanitized;
                // second proeprty of the value is the file content
            });
        }
        WriteFile("Other/Outputs/TypeScript/Source.ts", LuaContent);
        console.log(GetNiceTime(), "TypeScript Wrapper - Finished");
        console.log(GetNiceTime(), "Typescript-To-Lua - Transpilation begins");
    }
    Core.TypeScriptWrapper = TypeScriptWrapper;
})(Core || (Core = {}));
//#region Command Support
/** Retrive the value of a key=value pair passed as parameters */
function GetValue(key) {
    let Value;
    Args.forEach((val, index) => {
        if (!Value && val.indexOf(key) != -1) {
            Value = val.split("=")[1];
        }
    });
    return Value || undefined;
}
if (GetValue("Wrap") == "Lua") {
    Core.LuaWrapper();
}
else {
    Core.TypeScriptWrapper();
}
//#endregion
