

// Content // 

// Non elegant solution to type safety, global table and contextual local table with same name..

export namespace Core {
    export function LoadACR(this: void, name: string) {
        for (let i = 0; i < QtLib.ACRs.length; i++) {
            const thisACR = QtLib.ACRs[i];
            if (thisACR.GUI.name == name) {
                return thisACR;
            }
        }
    }
    export const Authenticated = true;
}
